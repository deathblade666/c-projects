#include <stdio.h>
#include <stdlib.h>

void betterCalc(){
    double num1;
    double num2;
    char op;

    printf("Enter Number: ");
    scanf("%lf", &num1);
    printf("Enter Operator: ");
    scanf(" %c", &op);
    printf("Enter second Number: ");
    scanf("%lf", &num2);

    if(op == '+'){
        printf("%f", num1 + num2);
    } else if(op == '-'){
        printf("%f", num1 - num2);
    } else if(op == '/'){
        printf("%f", num1 / num2);
    } else if(op == '*'){
        printf("%f", num1 * num2);
    } else{
        printf("Invalid Operator\n");
        printf("Valid Operators are  + - * /\n");
    }


}

void Calc(){
    double num1;
    double num2;
    printf("Enter the first number: ");
    scanf("%lf", &num1);
    printf("Enter the second number: ");
    scanf("%lf", &num2);
    printf("%f + %f = %f\n", num1, num2, num1 + num2);
}

void Fillintheblanks(){
    char color[20];
    char pluralNoun[20];
    char Fname[20];
    char Lname[20];

    printf("Enter a color: ");
    scanf("%s", color);
    printf("Enter a Plural Noun: ");
    scanf("%s", pluralNoun);
    printf("Enter a Name: ");
    scanf("%s%s", Fname, Lname);

    printf("%s is a color\n", color);
    printf("%s are dumb\n", pluralNoun);
    printf("%s %s is a person\n", Fname, Lname);
}

void Prompt(){
    char Answer;
    char yes[] = {'y', 'Y'};
    char no[] = {'n', 'N'};
    printf("Run Calculator? [Y/N] ");
    scanf(" %c", &Answer);
    if(Answer == yes[0] || Answer == yes[1]){
        Calc();
    } else if(Answer == no[0] || Answer == no[1]){
        printf("Run Word Game? [Y/N] ");
        scanf(" %c", &Answer);
        if(Answer == yes[0] || Answer == yes[1]){
            Fillintheblanks();
        } else if(Answer == no[1] || Answer == no[0]) {
            printf("Run better calculator? [Y/N] ");
            scanf(" %c", &Answer);
            if(Answer == yes [0] || Answer == yes[1]){
                betterCalc();
            } else if(Answer == no[1] || Answer == no[0]){
                printf("Goodbye\n");
            }
        }
    }
}


int main()
{
    Prompt();
    return 0;
}
